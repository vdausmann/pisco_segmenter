from setuptools import setup, find_packages

setup(
    name="pisco_segmenter",
    version="0.1",
    packages=find_packages(),
    author="Veit Dausmann, Tim Kalsberger",
    author_email="vdausmann@geomar.de",
    description="Segmentation algorithm for images produced by the Plankton Imaging System with Scanning Optics",
    license="MIT",
    keywords="keyword1 keyword2",
    url="https://codebase.helmholtz.cloud/vdausmann/pisco_segmenter",
)
