# Import necessary libraries.
import cv2 as cv
import numpy as np
import os
import time
import csv
from multiprocessing import Queue
from process_pool import ProcessPool
from dataclasses import dataclass

# Define a data class to hold the settings for detection.
@dataclass
class DetectionSettings:
    data_path: str  # Path to save data
    crop_path: str  # Path to save cropped images
    img_path: str  # Path to save images
    min_area_to_save: float  # Minimum area to save an image
    min_area_to_segment: float  # Minimum area to segment an image
    n_sigma: float  # Standard deviation multiplier for thresholding
    save_bb_image: bool  # Whether to save bounding box image
    save_crops: bool  # Whether to save cropped images
    equalize_hist: bool  # Whether to equalize histogram
    resize: bool  # Whether to resize images
    clear_save_path: bool  # Whether to clear save path
    mask_img: bool  # Whether to mask image
    mask_radius: int  # Mask radius

# Define a function to save crop data to a CSV file.
def save_crop_data(path, data):
    with open(path, "w", newline="") as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerows(data)

# Define a function to perform detection on an image.
def detect_on_img(input, settings: DetectionSettings, index=0):
    # Get the corrected image, the cleaned image, and the filename from the input.
    corrected, cleaned, _, fn = input

    # Convert the cleaned image to BGR color and resize it.
    color = cv.cvtColor(cleaned, cv.COLOR_GRAY2BGR)
    cleaned = cv.resize(cleaned, (5120, 5120))

    # Threshold the corrected image based on its mean and standard deviation.
    thresh = cv.threshold(
        corrected,
        np.mean(corrected) + settings.n_sigma * np.std(corrected),
        255,
        cv.THRESH_BINARY,
    )[1]
    thresh = thresh.astype(np.uint8)

    # Find contours in the thresholded image.
    cnts = cv.findContours(thresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)[0]
    areas = np.array([cv.contourArea(cnt) for cnt in cnts])

    # If the resize setting is True, scale up the areas by a factor of 4.
    if settings.resize:
        areas *= 4

    # Find the bounding boxes of the contours.
    bounding_boxes = [cv.boundingRect(cnt) for cnt in cnts]

    # Compute the center positions and match bounding box to full width image.
    bounding_boxes = np.array(
        [np.array((x + w / 2, y + h / 2, w, h)) for (x, y, w, h) in bounding_boxes]
    )
    if settings.resize:
        bounding_boxes *= 2  # Scale up the bounding boxes if the resize setting is True.

    # Initialize an empty list to hold the crop data.
    crop_data = []
    c = 0  # Initialize a counter for the crop data.

    # Process each contour and its corresponding area and bounding box.
    for i, cnt in enumerate(cnts):
        if areas[i] < settings.min_area_to_segment:
            continue  # Skip contours with small area.

        # Construct the filename for the crop.
        crop_fn = os.path.join(settings.crop_path, f"{fn[:-4]}_{c}.jpg")

        # Draw the contour on the color image if the save_bb_image setting is True.
        if settings.save_bb_image:
            cv.drawContours(color, [cnt], -1, (0, 255, 0), 2)

        # Get the bounding box of the contour.
        x, y, w, h = bounding_boxes[i]

        # If the area of the contour is greater than the minimum area to save, save the crop data and the crop.
        if areas[i] > settings.min_area_to_save:
            crop_data.append([c, os.path.basename(crop_fn), areas[i], x, y, w, h, 1])

            # Compute the bounding box coordinates.
            x = int(x - w / 2)
            y = int(y - h / 2)
            h = int(h)
            w = int(w)

            # Save the crop if the save_crops setting is True.
            if settings.save_crops:
                crop = cleaned[y : y + h, x : x + w]
                cv.imwrite(crop_fn, crop)
        else:
            crop_data.append([c, os.path.basename(crop_fn), areas[i], x, y, w, h, 0])

        c += 1  # Increment the counter.

    # Save the color image with drawn contours if the save_bb_image setting is True.
    if settings.save_bb_image:
        color = cv.resize(color, (1000, 1000))
        cv.imwrite(os.path.join(settings.img_path, fn), color)

    # Save the crop data to a CSV file.
    save_crop_data(os.path.join(settings.data_path, fn[:-4] + ".csv"), crop_data)


# Define a function to run detection on a queue of images.
def run_detection(input: Queue, settings: DetectionSettings, n_cores, n_imgs):
    print("starting detection")

    # Create a ProcessPool to perform detection on the images in parallel.
    pool = ProcessPool(
        lambda input, index: detect_on_img(input, settings, index),
        10,
    )
    pool.start(n_cores)

    # Add each image in the input queue to the pool as a task.
    for _ in range(n_imgs):
        task = input.get()
        pool.add_task(task)

    # Wait for all tasks in the pool to finish.
    pool.stop(slow=True)

    # Wait for the pool to finish in a different way.
    while pool.is_running():
        time.sleep(1)

    print("Detection finished")
