# Import necessary libraries.
import cv2 as cv
import numpy as np
import time
import os
import threading
from process_pool import ProcessPool
from multiprocessing import Manager
from thread_pool import ThreadPool

# Define a class to hold the outputs from reading the images.
class ReaderOutput:
    def __init__(self, n_images, manager: Manager) -> None:
        # Create a lock to ensure thread safety when adding outputs.
        self.lock = threading.Lock()
        # Initialize a list of None of the same length as the number of images.
        self.images = manager.list([None for _ in range(n_images)])

    # Define a method to add an output to the list.
    def add_output(self, img, fn, index):
        # Ensure thread safety using the lock.
        with self.lock:
            # Add the output at the correct index.
            self.images[index] = (img, fn)

# Define a function to read an image and add its output to the ReaderOutput.
def read_img(output: ReaderOutput, input, thread_index=0):
    file, img_index = input
    # Get the basename of the file.
    fn = os.path.basename(file)
    # Read the image in grayscale.
    img = cv.imread(file, cv.IMREAD_GRAYSCALE)
    # Resize the image to 2560x2560.
    img = cv.resize(img, (2560, 2560))
    # Add the image and its filename to the output.
    output.add_output(img, fn, img_index)

# Define a function to run the reader on a list of files.
def run_reader(files, output: ReaderOutput, n_threads: int):
    print("Starting run_reader")
    # Create a ThreadPool to read the images in parallel.
    pool = ThreadPool(lambda input, index: read_img(output, input, index), 100)
    pool.start(n_threads)
    for file in files:
        print(f"Adding file {file} to reader pool")
        try:
            # Add each file to the pool as a task.
            pool.add_task(file)
        except Exception as e:
            # If an exception occurs, print it.
            print(f"Exception when adding file {file} to reader pool: {e}")
    print("Waiting for reader pool to finish")
    # Wait for all tasks in the pool to finish.
    pool.stop(slow=True)
    print("Finished run_reader")
    
    # The following commented-out code can be used for batchwise processing for better performance.
    # while pool.is_running():
    #     time.sleep(1)
    # print("Reader finished")

