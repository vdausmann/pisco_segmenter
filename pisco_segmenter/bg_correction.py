# Import necessary libraries.
import time
import cv2 as cv
import numpy as np
from process_pool import ProcessPool
from multiprocessing import Queue
from reader import ReaderOutput

# Define a function to check if an image is ready to be processed.
def is_ready(img_index: int, input: ReaderOutput, n_bg_imgs):
    # Determine the starting index based on the number of background images.
    start = (
        img_index
        if img_index + n_bg_imgs < len(input.images)
        else len(input.images) - n_bg_imgs
    )
    # Check if all necessary images are available.
    for i in range(start, start + n_bg_imgs):
        if input.images[i] is None:
            return False
    return True

# Define a function to correct an image.
def correct_img(
    img_index: int, input: ReaderOutput, output: Queue, n_bg_imgs: int, index=0
):
    # Wait until the image is ready to be processed.
    while not is_ready(img_index, input, n_bg_imgs):
        time.sleep(0.1)
    # Get the image and its filename.
    img, fn = input.images[img_index]
    # Calculate the mean pixel value of the image.
    mean = np.mean(img)
    # Determine the starting index based on the number of background images.
    start = (
        img_index
        if img_index + n_bg_imgs < len(input.images)
        else len(input.images) - n_bg_imgs
    )
    # Calculate the maximum pixel value of the necessary images for background correction.
    bg = np.max([img[0] for img in input.images[start : start + n_bg_imgs]], axis=0)
    # Subtract the background from the image.
    correct_img = cv.absdiff(img, bg)
    # Subtract the mean pixel value from the corrected image.
    cleaned_img = cv.absdiff(correct_img, mean)
    # Put the corrected and cleaned images, the mean pixel value, and the filename in the output queue.
    output.put((correct_img, cleaned_img, mean, fn))

# Define a function to run the background correction on a list of images.
def run_bg_correction(input: ReaderOutput, output: Queue, n_bg_imgs: int):
    print("Starting run_bg_correction")
    # Create a ProcessPool to correct the images in parallel.
    pool = ProcessPool(
        lambda img_index, index: correct_img(
            img_index, input, output, n_bg_imgs, index
        ),
        -1,
    )
    pool.start(4)
    for i in range(len(input.images)):
        print(f"Adding image {i} to bg correction pool")
        try:
            # Add each image to the pool as a task.
            pool.add_task(i)
        except Exception as e:
            # If an exception occurs, print it.
            print(f"Exception when adding image {i} to bg correction pool: {e}")
    # Wait for all tasks in the pool to finish.
    pool.stop(slow=True)
    print("Finished run_bg_correction")
    
    # The following commented-out code can be used to wait for the pool to finish in a different way.
    # while pool.is_running():
    #     time.sleep(1)
    # print("Bg correction finished")
