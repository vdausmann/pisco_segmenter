# The necessary libraries are imported.
import cv2 as cv
import os
import time
from threading import Thread
from multiprocessing import Queue, Manager, Process
from reader import run_reader, ReaderOutput
from bg_correction import run_bg_correction
from deconvolution import run_deconvolution
from detection import run_detection, DetectionSettings

# Define a function to run the segmentation process.
def run_segmenter(src_path: str, save_path: str, deconvolution: bool):
    # Record the start time of the process.
    start = time.perf_counter()
    
    # Get the list of files in the source directory.
    files = os.listdir(src_path)
    
    # Try to sort the files by their timestamp which is assumed to be in the file name.
    try:
        files.sort(
            key=lambda x: float(os.path.basename(x).split("_")[0].split("-")[1])
        )  # sort after time
    except:
        # If the timestamp-based sorting fails, sort the files in the default (alphabetic) order.
        print("Key-based sort failed")
        files.sort()
    
    # Generate a list of tuples, each containing the full path to a file and its index in the sorted list.
    files = [(os.path.join(src_path, file), i) for i, file in enumerate(files)]

    # Create the directories where the results will be saved.
    save_path = os.path.join(save_path, os.path.basename(src_path))
    os.makedirs(save_path, exist_ok=True)
    crop_path = os.path.join(save_path, "Crops")
    data_path = os.path.join(save_path, "Data")
    img_path = os.path.join(save_path, "Images")
    os.makedirs(crop_path, exist_ok=True)
    os.makedirs(data_path, exist_ok=True)
    os.makedirs(img_path, exist_ok=True)

    # Create a Multithreading Manager object for sharing data across processes.
    manager = Manager()
    
    # Create a DetectionSettings object with the desired settings.
    settings = DetectionSettings(
        data_path,  #data_path: str
        crop_path,  #crop_path: str
        img_path,   #img_path: str
        400,        #min_area_to_save: float
        400,        #min_area_to_segment: float
        2,          #n_sigma: float
        True,       #save_bb_image: bool
        True,       #save_crops: bool
        True,       #equalize_hist: bool
        True,       #resize: bool
        True,       #clear_save_path: bool
        True,       #mask_img: bool
        3000,       #mask_radius: int
    )

    # Create a ReaderOutput object to keep track of the reading process.
    reader_output = ReaderOutput(len(files), manager)
    
    # Create two queues to hold the results of the background correction and deconvolution processes.
    bg_output_queue = Queue()
    deconv_output_queue = Queue()

    print("Starting reader")
    # Start a new thread to read the files.
    Thread(target=run_reader, args=(files, reader_output, 8)).start()
    print("Finished reader")

    print("Starting bg_correction")
    # Run the background correction process on the files.
    run_bg_correction(reader_output, bg_output_queue, 6)
    print("Finished bg_correction")

    print("Starting deconvolution")
    # If deconvolution is enabled, start a new thread to run the deconvolution process.
    if deconvolution:
        Thread(
            target=run_deconvolution,
            args=(bg_output_queue, deconv_output_queue, len(files)),
        ).start()
    else:
        # If deconvolution is not enabled, use the results of the background correction as input to the detection process.
        deconv_output_queue = bg_output_queue
    print("Finished deconvolution")

    print("Starting detection")
    # Run the detection process on the results of the deconvolution (or background correction).
    run_detection(deconv_output_queue, settings, 6, len(files))
    print("Finished detection")

    # Calculate and print the duration of the whole process and the average duration per file.
    end = time.perf_counter()
    duration = end - start
    print(duration, duration / len(files))


# If this script is being run directly (not imported as a module), start the segmentation process on a specified set of files.
if __name__ == "__main__":
    run_segmenter(
        "/home/plankton/Data/M181_test_set",
        "/home/plankton/Results/M181/segmentation_w_deconv/M181-175-1_CTD-050_00deg00S-019deg00W_20220509-0543/M181_test_set/",
        True, #deconvolution
    )
    print("Main script exiting")
