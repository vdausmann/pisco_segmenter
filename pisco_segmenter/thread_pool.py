# Import necessary libraries.
import threading
import time

# Define a class for a thread pool.
class ThreadPool:
    SLOW_STOP_KEY = "stop_pool"  # Define a key for slow stopping.

    # Initialize the thread pool with a function to run and a maximum number of tasks.
    def __init__(self, func, max_todo_len: int = 10) -> None:
        self.todo = []  # List to hold tasks to do.
        self.lock = threading.Lock()  # Lock to ensure thread safety.
        self.threads = []  # List to hold threads.
        self.run = True  # Flag to indicate whether the pool is running.
        self.func = func  # Function to run.
        self.max_todo_len = max_todo_len  # Maximum length of todo list.
        self.max_sleep_counter = 10000000  # Maximum sleep counter.

    # Define a worker thread that gets tasks from the todo list and runs the function on them.
    def worker(self, index: int):
        sleep_counter = 0  # Counter for sleep.
        while True:
            # If the sleep counter exceeds the maximum or the run flag is False, quit.
            if sleep_counter > self.max_sleep_counter or not self.run:
                print(f"Worker {index} quitting")
                break
            # Acquire the lock to ensure thread safety.
            self.lock.acquire()
            if len(self.todo) == 0:  # If there are no tasks to do, sleep.
                self.lock.release()
                sleep_counter += 1
                time.sleep(0.01)
            else:  # If there are tasks to do, reset the sleep counter and do a task.
                sleep_counter = 0
                job = self.todo.pop(0)
                if job == self.SLOW_STOP_KEY:  # If the task is the slow stop key, stop.
                    self.run = False
                    break
                # Release the lock and run the function on the task.
                self.lock.release()
                self.func(job, index)

    # Define a method to start the pool with a specified number of threads.
    def start(self, n_threads: int):
        for i in range(n_threads):
            t = threading.Thread(target=self.worker, args=(i,))
            t.start()
            self.threads.append(t)

    # Define a method to add a task to the pool.
    def add_task(self, job):
        while True:
            # Acquire the lock to ensure thread safety.
            self.lock.acquire()
            # If the todo list is not full, add the task and break.
            if len(self.todo) < self.max_todo_len:
                self.todo.append(job)
                self.lock.release()
                break
            # If the todo list is full, release the lock and sleep.
            self.lock.release()
            time.sleep(0.01)

    # Define a method to stop the pool.
    def stop(self, slow: bool = False):
        if slow:  # If slow is True, add the slow stop key to the todo list.
            self.todo.append(self.SLOW_STOP_KEY)
        else:  # If slow is False, set the run flag to False.
            self.run = False

    # Define a method to check whether the pool is running.
    def is_running(self):
        if self.run:  # If the run flag is True, return True.
            return True
        else:  # If the run flag is False, check if any thread is alive.
            for t in self.threads:
                if t.is_alive():
                    return True
            return False
