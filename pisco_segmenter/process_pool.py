# Import necessary libraries.
from multiprocessing import Process, Queue, Value
from queue import Empty
import time

# Define a class for a process pool.
class ProcessPool:
    # Initialize the process pool with a function to run and a maximum number of tasks.
    def __init__(self, func, max_tasks: int = 10) -> None:
        self.func = func  # The function to run.
        # Create a queue for the tasks. The size of the queue is determined by max_tasks.
        if max_tasks > 0:
            self.tasks = Queue(max_tasks)
        else:
            self.tasks = Queue()
        self.running = Value("i", 1)  # A flag to indicate whether the pool is running.

    # Define a method to add a task to the pool.
    def add_task(self, task):
        self.tasks.put(task)

    # Define a worker process that gets tasks from the queue and runs the function on them.
    def worker(self, index: int):
        while self.running.value == 1:
            try:
                task = self.tasks.get(timeout=1)
                if task == "quit":
                    self.running.value = 0
                    print(f"Process {index} received quit signal in process pool {id(self)}.")
                    break
                print(f"Process {index} starting task {task}.")
                self.func(task, index)
                print(f"Process {index} finished task {task}.")
            except Empty:
                print(f"Process {index} waiting for task.")
                if self.running.value == 0:
                    print(f"Process {index} exiting due to running flag set to 0.")
                    break
        print(f"Process {index} finished")

    # Define a method to start the pool with a specified number of processes.
    def start(self, n_processes):
        self.processes = []

        for i in range(n_processes):
            p = Process(target=self.worker, args=(i,))
            p.start()
            self.processes.append(p)
            print(f"Started process {i}.")

    # Define a method to stop the pool.
    def stop(self, slow: bool = True):
        if slow:
            self.add_task("quit")
            print(f"Added quit task to queue in process pool {id(self)}.")
        else:
            self.running.set(0)
            print(f"Set running flag to 0 in process pool {id(self)}.")

    # Define a method to check whether the pool is running.
    def is_running(self):
        running = self.running.value == 1
        process_statuses = [p.is_alive() for p in self.processes]
        print(f"Running flag: {running}")
        print(f"Process statuses: {process_statuses}")
        return running or any(process_statuses)

# Test the process pool.
if __name__ == "__main__":
    import time

    def f(x, i):
        print(f"Process {i} has the task {x}")
        time.sleep(1)

    pool = ProcessPool(f, 100)
    for i in range(100):
        pool.add_task(i)

    pool.start(5)
    pool.stop(True)
