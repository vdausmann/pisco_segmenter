# Import necessary libraries.
import cv2 as cv
import torch
import numpy as np
from multiprocessing import Queue
from lucyd import LUCYD, device
import os


# Load the LUCYD deconvolution model globally for faster inference on CPU.
# This is done once at the start of the program to avoid the overhead of loading the model multiple times.
MODEL_NAME='lucyd-psf-sim-2.pth'

# Get the directory of the current script.
script_dir = os.path.dirname(os.path.realpath(__file__))

# Form the path to the model file.
model_path = os.path.join(script_dir, 'models', MODEL_NAME)
model = LUCYD(num_res=1).to(device)
# Load the model.
model.load_state_dict(torch.load(model_path))
model.eval()  # Set the model to evaluation mode.

# Define a function to run the deconvolution on a queue of images.
def run_deconvolution(input: Queue, output: Queue, n_imgs: int):
    print("Starting deconvolution")
    # Process each image in the input queue.
    for _ in range(n_imgs):
        # Get the corrected image, the cleaned image, the mean pixel value, and the filename from the input queue.
        corrected, cleaned, mean, fn = input.get()

        # Perform deconvolution on the cleaned image.
        with torch.no_grad():  # Disable gradient computation to save memory and speed up computation.
            x = cleaned
            # Ensure the image dimensions are even.
            if x.shape[0] % 2 != 0: x = x[1:]
            if x.shape[1] % 2 != 0: x = x[:,1:]
            # Normalize the pixel values to the range 0-1.
            x = x/255

            # Convert the image to a torch tensor and add two singleton dimensions to match the model's input shape.
            x_t = torch.from_numpy(x).to(device)
            x_t = x_t.unsqueeze(0).unsqueeze(0)

            # Perform deconvolution.
            y_hat, y_k, update = model(x_t.float())
            # Convert the deconvolved image back to a numpy array and rescale the pixel values back to the range 0-255.
            deconv = y_hat.detach().cpu().numpy()[0,0]
            deconv = deconv*255

        # Replace the cleaned image with the deconvolved image.
        cleaned = deconv.astype(np.float32)
        # Compute the absolute difference between the deconvolved image and the mean pixel value.
        corrected = cv.absdiff(cleaned, mean)
        # Put the corrected image, the deconvolved image, the mean pixel value, and the filename in the output queue.
        output.put((corrected, cleaned, mean, fn))
        print("successfully deconvolved image:",fn)
    print("finished deconvolution")
